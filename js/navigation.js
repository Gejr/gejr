// Get Menu Items
const menuBurger = document.querySelector('.menu-burger');
const menu = document.querySelector('.menu');
const menuNav = document.querySelector('.menu-nav');
const menuBrand = document.querySelector('.menu-brand');
const menuPortrait = document.querySelector('.portrait');
const navItems = document.querySelectorAll('.nav-item');

// Initialize Menu State
let showMenu = false;

menuBurger.addEventListener('click', toggleMenu);

function toggleMenu() {
  if(!showMenu) {
    menuBurger.classList.add('close');
    menu.classList.add('show');
    menuNav.classList.add('show');
    menuBrand.classList.add('show');
    menuPortrait.classList.add('show');
    navItems.forEach(item => item.classList.add('show'));

    showMenu = true;
  } else {
    menuBurger.classList.remove('close');
    menu.classList.remove('show');
    menuNav.classList.remove('show');
    menuBrand.classList.remove('show');
    menuPortrait.classList.remove('show');
    navItems.forEach(item => item.classList.remove('show'));

    showMenu = false;
  }
}
